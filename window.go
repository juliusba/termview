package terminal

import (
	"fmt"
	"math"
	"sync"
	"terminal/styling"

	"github.com/nsf/termbox-go"
)

type Window struct {
	Container

	Views      viewList
	focusIndex int

	static bool

	lock sync.Mutex
}

func (w *Window) firstFreePosition(minWidth, minHeight int) (x, y int) {
	if minWidth > w.iWidth || minWidth == 0 {
		minWidth = w.iWidth / 4
	}
	if minHeight > w.iHeight || minHeight == 0 {
		minHeight = w.iHeight / 4
	}

	for _, view := range w.Views {
		viewX, viewY := view.Position()
		viewWidth, viewHeight := view.Size()

		if bottom := viewY + viewHeight; bottom > y {
			if right := viewX + viewWidth; right > x {
				x = right + 1
				if x+minWidth > w.width {
					x = 0
					y = bottom + 1
				}
				continue
			}
		}
	}

	return
}

func (w *Window) TableLayout(views viewList) {
	colSize := int(math.Max(float64(views.MinWidth())/4.0, 2.0))
	rowSize := int(math.Max(float64(views.MinHeight())/4.0, 2.0))
	cols := w.width / colSize
	rows := w.height / rowSize
	fmt.Println(colSize, rowSize, cols, rows, w.width, w.height)
	aspectRatio := float64(cols) / float64(rows)

	layout := make([][]bool, rows)
	for i := 0; i < rows; i++ {
		layout[i] = make([]bool, cols)
	}

	views.sortBySize()

	positions := make([]struct{ col, row int }, len(views))
	potentialPositions := make([]struct{ col, row int }, 1, 4)
	potentialPositions[0] = struct{ col, row int }{0, 0}

viewLoop:
	for i, view := range views {
		viewCols := int(math.Ceil(float64(view.MinWidth()) / float64(colSize)))
		viewRows := int(math.Ceil(float64(view.MinHeight()) / float64(rowSize)))
		bestCols := cols
		bestIndex := 0

	posLoop:
		for i, position := range potentialPositions {
			minCols := position.col + viewCols
			minRows := position.row + viewRows
			minCols = int(math.Ceil(math.Max(float64(minCols), float64(minRows)*aspectRatio)))

			// Continue if a better solution is already found
			if minCols > bestCols {
				continue posLoop
			}

			// Continue if rest of width is to small
			if minCols > cols {
				continue posLoop
			}

			// Test if space is big enough.
			for r, lastRow := position.row, position.row+viewRows; r < lastRow; r++ {
				for c, lastCol := position.col, position.col+viewCols; c < lastCol; c++ {
					if layout[r][c] {
						continue posLoop
					}
				}
			}

			bestIndex = i
			bestCols = minCols
		}

		bestPos := potentialPositions[bestIndex]
		positions[i] = bestPos

		// Set space as occupied.
		for r, lastRow := bestPos.row, bestPos.row+viewRows; r < lastRow; r++ {
			for c, lastCol := bestPos.col, bestPos.col+viewCols; c < lastCol; c++ {
				layout[r][c] = true
			}
		}

		// Remove taken position.
		potentialPositions[bestIndex] = potentialPositions[len(potentialPositions)-1]
		potentialPositions = potentialPositions[:len(potentialPositions)-1]

		// Add new positions (to the right and beneith) given they are not already taken,
		// and that they are not dominated by other positions (worse x and y).
		rightPos := struct{ col, row int }{bestPos.col + viewCols, bestPos.row}
		if rightPos.col == cols || layout[rightPos.row][rightPos.col] {
			goto beneathPos
		}
		for _, pos := range potentialPositions {
			if pos.col <= rightPos.col && pos.row <= rightPos.row {
				goto beneathPos
			}
		}
		potentialPositions = append(potentialPositions, rightPos)

	beneathPos:
		beneathPos := struct{ col, row int }{bestPos.col, bestPos.row + viewRows}
		if len(layout) <= beneathPos.row {
			temp := layout
			layout = make([][]bool, beneathPos.row+1)
			i := 0
			for ; i < len(temp); i++ {
				layout[i] = temp[i]
			}
			for ; i < len(layout); i++ {
				layout[i] = make([]bool, cols)
			}
		}
		if layout[beneathPos.row][beneathPos.col] {
			continue viewLoop
		}
		for _, pos := range potentialPositions {
			if pos.col <= beneathPos.col && pos.row <= beneathPos.row {
				continue viewLoop
			}
		}
		potentialPositions = append(potentialPositions, beneathPos)
	}

	// Calculate scaling factor
	lastUsedCol, lastUsedRow := 0, 0
	for i, view := range views {
		col := positions[i].col + int(math.Ceil(float64(view.MinWidth())/float64(colSize)))
		row := positions[i].row + int(math.Ceil(float64(view.MinHeight())/float64(rowSize)))
		if col > lastUsedCol {
			lastUsedCol = col
		}
		if row > lastUsedRow {
			lastUsedRow = row
		}
	}
	widthScaling := float64(cols) / float64(lastUsedCol)
	heightScaling := float64(rows) / float64(lastUsedRow)

	// Set new positions and sizes.
	for i, view := range views {
		width := int(float64(colSize) * math.Ceil(float64(view.MinWidth())/float64(colSize)) * widthScaling)
		height := int(float64(rowSize) * math.Ceil(float64(view.MinHeight())/float64(rowSize)) * heightScaling)
		view.SetSize(width, height)
		view.SetPosition(int(float64(positions[i].col)*widthScaling*float64(colSize)),
			int(float64(positions[i].row)*heightScaling*float64(rowSize)))
	}
}

func (w *Window) AddView(view View) {
	// TODO: lock to make sure its all in sync.
	// As of now I roundtrip the renderer by assigning to w.views after calculating new layout.
	if w.static {
		x, y := w.firstFreePosition(view.MinSize())
		view.SetSize(view.MinSize())
		view.SetPosition(x, y)
		w.Views = append(w.Views, view)
	} else {
		w.lock.Lock()
		defer w.lock.Unlock()
		views := append(w.Views, view)
		w.TableLayout(views)
		w.Views = views
	}
}

func (w *Window) Render() {
	w.lock.Lock()
	defer w.lock.Unlock()
	for _, view := range w.Views {
		view.Render()
		cells := view.CellBuffer()
		vX, vY := view.Position()
		vWidth, vHeight := view.Size()

		w.content.writeInnerBuffer(cells, w.iWidth, vWidth, vHeight, vX, vY)
	}
	w._renderContent = true
	w.Container.Render()
}

func (w *Window) KeyEvent(key termbox.Key) bool {
	if w.focusIndex != -1 && w.Views[w.focusIndex].KeyEvent(key) {
		return true
	}

	switch key {
	case termbox.KeyArrowLeft:
		if w.focusIndex <= 0 {
			w.changeFocusIndex(len(w.Views) - 1)
		} else {
			newFocusIndex := (w.focusIndex - 1) % len(w.Views)
			w.changeFocusIndex(newFocusIndex)
		}
		return true
	case termbox.KeyArrowRight:
		newFocusIndex := (w.focusIndex + 1) % len(w.Views)
		w.changeFocusIndex(newFocusIndex)
		return true
	}

	return w.Container.KeyEvent(key)
}

func (w *Window) changeFocusIndex(newIndex int) {
	if w.focusIndex != -1 {
		w.Views[w.focusIndex].SetFocus(false)
	}
	w.focusIndex = newIndex
	w.Views[w.focusIndex].SetFocus(true)
}

func NewWindow(width, height int) *Window {
	w := new(Window)
	w.focusIndex = -1
	w.SetStyle(styling.Style{
		Borders:    false,
		Scrollable: false,
		MarginX:    0,
		MarginY:    0,
		PaddingX:   0,
		PaddingY:   0,
		Fg:         termbox.ColorDefault,
		Bg:         termbox.ColorDefault,
	})
	w.SetSize(width, height)

	w.Views = make([]View, 0, 2)

	return w
}
