package terminal

import (
	"math"
	"sort"
)

type viewList []View

// MinWidth returns the smallest minWidth
func (views viewList) MinWidth() int {
	minWidth := math.MaxInt64
	for _, view := range views {
		if view.MinWidth() < minWidth {
			minWidth = view.MinWidth()
		}
	}
	return minWidth
}

// MinHeight returns the smallest minWidth
func (views viewList) MinHeight() int {
	minHeight := math.MaxInt64
	for _, view := range views {
		if view.MinHeight() < minHeight {
			minHeight = view.MinHeight()
		}
	}
	return minHeight
}

/*------------------------------------------------------------------------------------------*/
/*--------------------------------- Sort views by width ------------------------------------*/
/*------------------------------------------------------------------------------------------*/

type _viewsByWidth []View

func (views _viewsByWidth) Less(i, j int) bool {
	return views[i].Width() < views[j].Width()
}

func (views _viewsByWidth) Len() int {
	return len(views)
}

func (views _viewsByWidth) Swap(i, j int) {
	temp := views[i]
	views[i] = views[j]
	views[j] = temp
}

func (views viewList) sortByWidth() {
	sort.Sort(_viewsByWidth(views))
}

/*------------------------------------------------------------------------------------------*/
/*--------------------------------- Sort views by height -----------------------------------*/
/*------------------------------------------------------------------------------------------*/

type _viewsByHeight []View

func (views _viewsByHeight) Less(i, j int) bool {
	return views[i].Height() < views[j].Height()
}

func (views _viewsByHeight) Len() int {
	return len(views)
}

func (views _viewsByHeight) Swap(i, j int) {
	temp := views[i]
	views[i] = views[j]
	views[j] = temp
}

func (views viewList) sortByHeight() {
	sort.Sort(_viewsByHeight(views))
}

/*------------------------------------------------------------------------------------------*/
/*---------------------------------- Sort views by size ------------------------------------*/
/*------------------------------------------------------------------------------------------*/

type _viewsBySize []View

func (views _viewsBySize) Less(i, j int) bool {
	return views[i].Height()*views[i].Width() > views[j].Height()*views[j].Width()
}

func (views _viewsBySize) Len() int {
	return len(views)
}

func (views _viewsBySize) Swap(i, j int) {
	temp := views[i]
	views[i] = views[j]
	views[j] = temp
}

func (views viewList) sortBySize() {
	sort.Sort(_viewsBySize(views))
}
