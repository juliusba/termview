package terminal

import (
	"runtime"
	"time"
)

type RuntimeStats struct {
	CPUs       int
	Memory     int
	GoRoutines int
}

func (rtStats *RuntimeStats) Update(elapsed time.Time) {
	rtStats.CPUs = runtime.NumCPU()
	rtStats.GoRoutines = runtime.NumGoroutine()
}

type RuntimeView struct {
	ObjectView

	runtimeStats RuntimeStats
}

func NewRuntimeView() *RuntimeView {
	rtView := new(RuntimeView)
	rtView.runtimeStats.Update(time.Time{})
	rtView.ObjectView = *NewObjectView("Runtime", &rtView.runtimeStats)

	return rtView
}
