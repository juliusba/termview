package terminal

import "github.com/nsf/termbox-go"

type cellBuffer []termbox.Cell

// extend adds capacity to content if needed.
func (buffer cellBuffer) extendIf(newLen int) cellBuffer {
	if len(buffer) < newLen {
		if cap(buffer) < newLen {
			newBuffer := make(cellBuffer, newLen, int(float64(newLen)*1.3))
			copy(newBuffer, buffer)
			return newBuffer
		}
		return buffer[:newLen]
	}
	return buffer
}

func (buffer cellBuffer) write(index int, cells cellBuffer) cellBuffer {
	buffer = buffer.extendIf(index + len(cells))

	for i := 0; i < len(cells); i++ {
		buffer[index] = cells[i]
		index++
	}
	return buffer
}

func (buffer cellBuffer) writeString(index int, str string, fg, bg termbox.Attribute) cellBuffer {
	runes := []rune(str)
	buffer = buffer.extendIf(len(runes) + len(buffer))
	for i := 0; i < len(runes); i++ {
		buffer[index].Fg = fg
		buffer[index].Bg = bg
		buffer[index].Ch = runes[i]
		index++
	}
	return buffer
}

func (buffer cellBuffer) appendString(str string, fg, bg termbox.Attribute) cellBuffer {
	index := len(buffer)
	return buffer.writeString(index, str, fg, bg)
}

func (buffer cellBuffer) padd(count int, fg, bg termbox.Attribute) cellBuffer {
	runes := make([]rune, count)
	for i := 0; i < count; i++ {
		runes[i] = ' '
	}
	return buffer.appendString(string(runes), fg, bg)
}

func (outerBuffer cellBuffer) writeInnerBuffer(innerBuffer cellBuffer, oWidth, iWidth, iHeight, iX, iY int) {
	buffIndex := iY*oWidth + iX
	contentIndex := 0
	for iRow := 0; iRow < iHeight; iRow++ {
		for iCol := 0; iCol < iWidth; iCol++ {
			outerBuffer[buffIndex] = innerBuffer[contentIndex]

			contentIndex++
			buffIndex++
		}
		buffIndex += oWidth - iWidth
	}
}
