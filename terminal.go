package terminal

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/nsf/termbox-go"
)

var (
	window *Window

	stdoutOs    = os.Stdout
	stdoutRead  *os.File
	stdoutWrite *os.File
	stdoutBox   *Logger

	quit       chan int
	renderChan chan int
	sigChan    chan os.Signal
	cmdChan    chan struct {
		name string
		args []interface{}
	}

	lock sync.Mutex
)

func Init() {
	lock.Lock()
	defer lock.Unlock()

	if termbox.IsInit {
		return
	}

	err := termbox.Init()
	if err != nil {
		panic(err)
	}

	width, height := termbox.Size()
	window = NewWindow(width, height)

	// Redirect stdout
	stdoutRead, stdoutWrite, err = os.Pipe()
	if err != nil {
		panic(err)
	}
	os.Stdout = stdoutWrite

	// register signals to channel
	sigChan = make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	go func() {
		signal := <-sigChan
		Close()
		fmt.Printf("Have signal: \n%s", signal)
	}()

	renderChan = make(chan int)

	stdoutBox = NewLogger("Stdout")
	window.AddView(stdoutBox)

	go _printStdout()
	go _pollEvents()
	go _render()
}

func UserExit() {
	PollCommand(nil)
}

func PollCommand(commands map[string]func(args ...interface{})) {
	lock.Lock()
	defer lock.Unlock()

	if !termbox.IsInit {
		fmt.Println("Trying to PollCommand, but TermView is not inited.")
		return
	}

	cmdChan = make(chan struct {
		name string
		args []interface{}
	})

	lock.Unlock()

	if commands == nil {
		commands = make(map[string]func(args ...interface{}))
	}
	for cmd := range cmdChan {
		if fn, ok := commands[cmd.name]; ok {
			fn(cmd.args...)
		}
	}
}

func Close() {
	lock.Lock()
	defer lock.Unlock()

	if !termbox.IsInit {
		fmt.Println("Trying to close TermView, but it is already closed.")
		return
	}

	fmt.Println("Closing TermView...")
	termbox.Interrupt()
	close(renderChan)
	os.Stdout = stdoutOs
	stdoutRead.Close()
	stdoutWrite.Close()
	termbox.Close()
	if cmdChan != nil {
		close(cmdChan)
	}

	time.Sleep(250 * time.Millisecond)

	fmt.Println("Closed TermView!")
}

func _render() {
	defer func() {
		if err := recover(); err != nil {
			Close()
			fmt.Println("Renderer paniced:", err)
		}
	}()

	i := 0
	for {
		select {
		case <-renderChan:
			fmt.Println("Stopping renderer...")
			return
		case <-time.After(75 * time.Millisecond):
			window.Render()
			cells := window.CellBuffer()
			backBuffer := termbox.CellBuffer()
			copy(backBuffer, cells)
			termbox.SetCell(i%window.width, window.height-1, 'A', termbox.ColorGreen, termbox.ColorGreen)
			termbox.Flush()
			i++
		}
	}
}

func _printStdout() {
	scanner := bufio.NewScanner(stdoutRead)
	for scanner.Scan() {
		line := scanner.Text()
		stdoutBox.Println(line)
	}
	fmt.Println("Stopping stdout...")
}

func _pollEvents() {
	for {
		evt := termbox.PollEvent()
		fmt.Println(evt)
		switch evt.Type {
		case termbox.EventInterrupt:
			fmt.Println("Stopping eventPoller...")
			return
		case termbox.EventKey:
			if !window.KeyEvent(evt.Key) {
				switch evt.Key {
				case termbox.KeyCtrlC, termbox.KeyCtrlZ:
					fmt.Println("Received interupt signal from user...")
					go Close()
				}
			}
		}
	}
}
