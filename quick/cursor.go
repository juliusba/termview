package terminal

import (
	"bufio"
	"fmt"
	"os"
)

func HideCursor() string {
	return SetCursorPos(-1, -1)
}

func GetCursorPos() (line, column int) {
	fmt.Println("\033[6n")
	bio := bufio.NewReader(os.Stdin)
	bytes, hasMoreInLine, err := bio.ReadLine()
	fmt.Println(string(bytes), hasMoreInLine, err)
	return
}

func SetCursorPos(line, column int) string {
	return fmt.Sprintf("\033[0%d;%dH", line, column)
}

func MoveCursorUp(lines int) string {
	return fmt.Sprintf("\033[%dA", lines)
}

func MoveCursorDown(lines int) string {
	return fmt.Sprintf("\033[%dB", lines)
}

func MoveCursorRight(columns int) string {
	return fmt.Sprintf("\033[%dC", columns)
}

func MoveCursorLeft(columns int) string {
	return fmt.Sprintf("\033[%dD", columns)
}

func Clear() string {
	return "\033[2J"
}

func ClearLine() string {
	return "\033[K"
}

func SaveCursorPosition() string {
	return "\033[s"
}

func RestoreCursorPosition() string {
	return "\033[u"
}
