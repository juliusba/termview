package terminal

import "fmt"

func Green(str string) string {
	return fmt.Sprint("\\e[0;32m", str, "\\e[0m")
}

func Red(str string) string {
	return fmt.Sprint("\\e[0;31m", str, "\\e[0m")
}

func Blue(str string) string {
	return fmt.Sprint("\\e[0;34m", str, "\\e[0m")
}

func Yellow(str string) string {
	return fmt.Sprint("\\e[0;33m", str, "\\e[0m")
}

func Purple(str string) string {
	return fmt.Sprint("\\e[0;35m", str, "\\e[0m")
}

func BoldWhite(str string) string {
	return fmt.Sprint("\\e[1;37m", str, "\\e[0m")
}

func BoldGreen(str string) string {
	return fmt.Sprint("\\e[1;32m", str, "\\e[0m")
}

func BoldRed(str string) string {
	return fmt.Sprint("\\e[1;31m", str, "\\e[0m")
}

func BoldBlue(str string) string {
	return fmt.Sprint("\\e[1;34m", str, "\\e[0m")
}

func BoldYellow(str string) string {
	return fmt.Sprint("\\e[1;33m", str, "\\e[0m")
}

func BoldPurple(str string) string {
	return fmt.Sprint("\\e[1;35m", str, "\\e[0m")
}
