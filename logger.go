package terminal

import (
	"fmt"

	"terminal/styling"

	"github.com/nsf/termbox-go"
)

type Logger struct {
	Container

	cursorPos int

	lines []cellBuffer
}

func (l *Logger) Println(args ...interface{}) {
	fg, bg := l.Style.Fg, l.Style.Bg
	firstArg := true
	var line cellBuffer
	for _, arg := range args {
		var str string
		switch arg.(type) {
		default:
			str = fmt.Sprint(arg)
		case termbox.Attribute:
			fg = arg.(termbox.Attribute)
			break
		}

		if firstArg {
			firstArg = false
		} else {
			str = string(append([]byte(" "), str...))
		}

		line = line.appendString(str, fg, bg)
	}

	l.lines = append(l.lines, line)

	l.renderLine(line)
	l._renderContent = true
}

func (l *Logger) Clear() {
	l.lines = l.lines[:0]
	l.cursorPos = 0
	l.reset()
}

func (l *Logger) SetSize(width, height int) {
	l.Container.SetSize(width, height)
	l.reset()
}

func (l *Logger) reset() {
	l.cursorPos = 0
	l.content = make(cellBuffer, l.iHeight*l.iWidth)
	for _, line := range l.lines {
		l.renderLine(line)
	}
	l._renderContent = true
}

func (l *Logger) renderLine(line cellBuffer) {
	cursorIsVisible := l.lastVisibleCell >= l.cursorPos
	l.content = l.content.extendIf(l.cursorPos + len(line) + (l.iWidth - (l.cursorPos+len(line)-1)%l.iWidth))
	l.content = l.content.write(l.cursorPos, line)
	l.cursorPos += len(line)
	padding := l.iWidth - (l.cursorPos % l.iWidth)
	l.content = l.content.padd(padding, l.Style.Fg, l.Style.Bg)
	l.cursorPos += padding

	if cursorIsVisible && l.lastVisibleCell < l.cursorPos {
		// Overkill, accurate number will be found anyway.
		l.ScrollDown(10000)
	}
}

func NewLogger(name string) *Logger {
	l := new(Logger)
	l.SetTitle(name)
	l.SetStyle(styling.DefaultStyle)
	l.minWidth = 75
	l.minHeight = 25

	return l
}

func AddLogger(name string) *Logger {
	logger := NewLogger(name)
	window.AddView(logger)

	return logger
}
