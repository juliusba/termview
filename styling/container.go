package styling

import "github.com/nsf/termbox-go"

var (
	DefaultStyle = Style{
		Borders:    true,
		Scrollable: true,

		MarginX:  2,
		MarginY:  1,
		PaddingX: 1,
		PaddingY: 0,

		Fg: termbox.ColorDefault,
		Bg: termbox.ColorDefault,
	}

	BorderFg        = termbox.ColorGreen
	BorderFgFocused = termbox.ColorCyan
)

type Style struct {
	Borders    bool
	Scrollable bool

	MarginX, MarginY   int
	PaddingX, PaddingY int
	Bg, Fg             termbox.Attribute

	Focused bool
}

func borderCell(focused bool) (cell termbox.Cell) {
	if focused {
		cell.Fg = BorderFgFocused
	} else {
		cell.Fg = BorderFg
	}
	return
}

func BorderCellCorner(focused bool) (cell termbox.Cell) {
	cell = borderCell(focused)
	cell.Ch = '+'
	return
}

func BorderCellHorizontal(focused bool) (cell termbox.Cell) {
	cell = borderCell(focused)
	cell.Ch = '-'
	return
}

func BorderCellVertical(focused bool) (cell termbox.Cell) {
	cell = borderCell(focused)
	cell.Ch = '|'
	return
}

func ScrollbarCell(focused bool) (cell termbox.Cell) {
	cell = termbox.Cell{
		Fg: termbox.ColorCyan,
		Bg: BorderFg,
		Ch: '|',
	}
	if focused {
		cell.Bg = BorderFgFocused
	}
	return
}
