package terminal

import (
	"fmt"
	"terminal/styling"

	"github.com/nsf/termbox-go"
)

// Container is used as an abstract class that most views will extend.
// It draws borders and makes sure that rendering is done correctly.
type Container struct {
	X, Y                int
	width, height       int
	minWidth, minHeight int

	iX, iY                int
	iWidth, iHeight       int
	iMinWidth, iMinHeight int

	Name string

	Style styling.Style

	Scroll struct {
		Enabled bool
		Visible bool
		index   int
		start   int
		end     int
	}

	parentView View

	cells   cellBuffer
	content cellBuffer

	firstVisibleCell int
	lastVisibleCell  int

	_renderAll       bool
	_renderTitle     bool
	_renderBorders   bool
	_renderScrollbar bool
	_renderContent   bool
}

/*-----------------------------------------------------------------------------------------------*/
/*------------------------------------- Public methods ------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*/

func (c *Container) CellBuffer() cellBuffer {
	return c.cells
}

func (c *Container) SetTitle(title string) {
	if c.Name == "" {
		if c.iY != 0 {
			c.iY--
		}
		if c.iHeight != 0 {
			c.iHeight--
		}
	}
	c.Name = title

	c._renderBorders = true
	c._renderTitle = true
}

func (c *Container) MinWidth() (minWidth int) {
	return c.minWidth
}

func (c *Container) MinHeight() (minHeight int) {
	return c.minHeight
}

func (c *Container) MinSize() (minWidth, minHeight int) {
	return c.minWidth, c.minHeight
}

func (c *Container) Width() (width int) {
	return c.width
}

func (c *Container) Height() (height int) {
	return c.height
}

func (c *Container) Size() (width, height int) {
	return c.width, c.height
}

func (c *Container) SetSize(width, height int) {
	c.width = width
	c.height = height

	c.iWidth = c.width - 2*(c.Style.MarginX+c.Style.PaddingX) + 1
	c.iHeight = c.height - 2*(c.Style.MarginY+c.Style.PaddingY) - 1
	if c.Style.Borders {
		c.iWidth -= 2
		c.iHeight -= 2
	}
	if c.Name != "" {
		c.iHeight--
	}
	c.lastVisibleCell = c.iHeight * c.iWidth
	c.cells = make(cellBuffer, c.height*c.width)
	c.content = make(cellBuffer, c.lastVisibleCell)

	c._renderAll = true
}

func (c *Container) Position() (x, y int) {
	return c.X, c.Y
}

func (c *Container) SetPosition(x, y int) {
	c.X = x
	c.Y = y
}

func (c *Container) SetStyle(style styling.Style) {
	// Adjust inner startX and inner startY
	c.iX = style.MarginX + style.PaddingX
	c.iY = style.MarginY + style.PaddingY
	if style.Borders {
		c.iX++
		c.iY++
	}
	if c.Name != "" {
		c.iY++
	}

	// If width and heigt is set, adjust them to the new style
	if c.width > 0 && c.height > 0 {
		c.iWidth = c.width - 2*(style.MarginX+style.PaddingX) + 1
		c.iHeight = c.height - 2*(style.MarginY+style.PaddingY) - 1

		if style.Borders {
			c.iWidth -= 2
			c.iHeight -= 2
		}
		if c.Name != "" {
			c.iHeight--
		}

		c._renderAll = true
	}

	c.Style = style
}

func (c *Container) KeyEvent(key termbox.Key) bool {
	switch key {
	default:
		return false
	case termbox.KeyArrowUp:
		c.ScrollUp(1)
		return true
	case termbox.KeyArrowDown:
		c.ScrollDown(1)
		return true
	}
}

func (c *Container) ScrollUp(lines int) {
	c._renderContent = true
	c._renderScrollbar = true

	c.Scroll.index -= lines
	if c.Scroll.index < 0 {
		c.Scroll.index = 0
		c.firstVisibleCell = 0
		c.lastVisibleCell = c.iHeight * c.iWidth
	} else {
		c.firstVisibleCell = c.Scroll.index * c.iWidth
		c.lastVisibleCell = (c.Scroll.index + c.iHeight) * c.iWidth
	}
}

func (c *Container) ScrollDown(lines int) {
	c._renderContent = true
	c._renderScrollbar = true

	c.Scroll.index += lines
	if c.Scroll.index+c.iHeight >= len(c.content)/c.iWidth {
		c.Scroll.index = len(c.content)/c.iWidth - c.iHeight
		c.firstVisibleCell = c.Scroll.index * c.iWidth
		c.lastVisibleCell = (c.Scroll.index + c.iHeight) * c.iWidth
	} else {
		c.firstVisibleCell = c.Scroll.index * c.iWidth
		c.lastVisibleCell = (c.Scroll.index + c.iHeight) * c.iWidth
	}
}

func (c *Container) SetFocus(focus bool) {
	c.Style.Focused = focus
	c._renderAll = true
}

/*-----------------------------------------------------------------------------------------------*/
/*------------------------------------- render methods ------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*/

func (c *Container) Render() {
	if c._renderAll {
		if c.Name != "" {
			c.renderTitle()
		}
		if c.Style.Borders {
			c.renderBorders()
		}
		c.renderContent()
		if c.Scroll.Enabled {
			c.renderScrollbar()
		}
		return
	}

	if c._renderTitle && c.Name != "" {
		c.renderTitle()
	}
	if c._renderBorders && c.Style.Borders {
		c.renderBorders()
	}
	if c._renderContent {
		c.renderContent()
	}
	if c._renderScrollbar && c.Scroll.Enabled {
		c.renderScrollbar()
	}
}

func (c *Container) renderTitle() {
	c._renderTitle = false

	if c.Name != "" {
		index := c.Style.MarginY*c.width + c.Style.MarginX + 1 + c.Style.PaddingX
		c.cells = c.cells.writeString(index, c.Name, styling.BorderFg, termbox.ColorDefault)
	}
}

func (c *Container) renderBorders() {
	c._renderBorders = false

	tl := c.Style.MarginY*c.width + c.Style.MarginX
	if c.Name != "" {
		tl += c.width
	}
	tr := tl + c.width - 2*c.Style.MarginX
	bl := c.height*c.width - tr
	br := bl + (tr - tl)

	cornerCell := styling.BorderCellCorner(c.Style.Focused)
	c.cells[tl] = cornerCell
	c.cells[tr] = cornerCell
	c.cells[bl] = cornerCell
	c.cells[br] = cornerCell

	horizontalCell := styling.BorderCellHorizontal(c.Style.Focused)
	for t := tl + 1; t < tr; t++ {
		c.cells[t] = horizontalCell
	}
	for b := bl + 1; b < br; b++ {
		c.cells[b] = horizontalCell
	}

	verticalCell := styling.BorderCellVertical(c.Style.Focused)
	for l := tl + c.width; l < bl; l += c.width {
		c.cells[l] = verticalCell
	}
	for r := tr + c.width; r < br; r += c.width {
		c.cells[r] = verticalCell
	}
}

func (c *Container) renderContent() {
	c._renderContent = false

	// Calculate new scrollbar position
	cHeight := len(c.content) / c.iWidth
	if c.Name == "EA log" {
		fmt.Println(c.Name, c.width > c.iWidth, cHeight, c.iHeight)
	}
	if cHeight > c.iHeight {
		c.Scroll.Visible = true
		lastScrollIndex := cHeight - c.iHeight

		scrollBarLen := int((float64(c.iHeight)/float64(cHeight))*float64(c.iHeight) + 0.5)
		if scrollBarLen == 0 {
			scrollBarLen = 1
		}
		newScrollBarStart := 0
		if c.Scroll.index == lastScrollIndex {
			newScrollBarStart += c.iHeight + (2 * c.Style.PaddingY) - scrollBarLen
		} else {
			newScrollBarStart += int((float64(c.Scroll.index)/float64(lastScrollIndex))*
				float64(c.iHeight-scrollBarLen) +
				0.5)
		}
		newScrollBarEnd := newScrollBarStart + scrollBarLen
		if newScrollBarStart != c.Scroll.start || newScrollBarEnd != c.Scroll.end {
			c.Scroll.start = newScrollBarStart
			c.Scroll.end = newScrollBarEnd
			c._renderScrollbar = true
		}
	} else if c.Scroll.Visible {
		c.Scroll.Visible = false
		c.Scroll.start = 0
		c.Scroll.end = 0
		c._renderScrollbar = true
	}

	// Render content. TODO: Add if clause. Check if content is nil
	c.cells.writeInnerBuffer(c.content[c.firstVisibleCell:c.lastVisibleCell], c.width, c.iWidth, c.iHeight, c.iX, c.iY)
}

func (c *Container) renderScrollbar() {
	c._renderScrollbar = false

	emptyCell := styling.BorderCellVertical(c.Style.Focused)
	scrollBarCell := styling.ScrollbarCell(c.Style.Focused)

	buffIndex := (c.iY+1)*c.width - c.Style.MarginX
	i := 0
	if c.Scroll.Visible {
		for ; i < c.Scroll.start; i++ {
			c.cells[buffIndex] = emptyCell
			buffIndex += c.width
		}
		for ; i < c.Scroll.end; i++ {
			c.cells[buffIndex] = scrollBarCell
			buffIndex += c.width
		}
		for endY := c.iHeight; i < endY; i++ {
			c.cells[buffIndex] = emptyCell
			buffIndex += c.width
		}
	} else {
		for endY := c.iHeight; i < endY; i++ {
			c.cells[buffIndex] = emptyCell
			buffIndex += c.width
		}
	}
}
