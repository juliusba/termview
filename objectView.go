package terminal

import (
	"log"
	"reflect"
	"terminal/styling"
)

type attribute struct {
	name  string
	value interface{}

	_render bool
}

type ObjectView struct {
	Container

	object     interface{}
	attributes []attribute
}

func NewObjectView(name string, object interface{}) *ObjectView {
	ov := new(ObjectView)
	ov.object = object
	e := ov.getElem()
	t := e.Type()

	if name == "" {
		ov.SetTitle(t.Name())
	} else {
		ov.SetTitle(name)
	}

	// Register attributes
	ov.attributes = make([]attribute, e.NumField())
	for i := 0; i < e.NumField(); i++ {
		name := t.Field(i).Name
		val := e.Field(i).Interface()

		ov.attributes[i] = attribute{name, val, false}
	}

	ov.SetStyle(styling.DefaultStyle)
	ov.minWidth = 30
	ov.minHeight = len(ov.attributes) + 2*(1+ov.Style.MarginY+ov.Style.PaddingY) + 1

	return ov
}

func (ov *ObjectView) Render() {
	// TODO
}

func (ov *ObjectView) updateAttributes() {
	e := ov.getElem()
	for i := 0; i < len(ov.attributes); i++ {
		val := e.Field(i).Interface()
		if ov.attributes[i]._render = ov.attributes[i].value != val; ov.attributes[i]._render {
			ov.attributes[i].value = val
		}
	}
}

func (ov *ObjectView) getElem() reflect.Value {
	v := reflect.ValueOf(ov.object)
	e := v.Elem()

	switch e.Kind() {
	case reflect.Ptr, reflect.Interface:
		e = e.Elem()
		if !e.IsValid() {
			log.Fatal("ObjView(", ov.Name, "): object is not valid")
		}
	}

	return e
}
