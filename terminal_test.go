package terminal

import (
	"fmt"
	"testing"
	"time"

	"github.com/nsf/termbox-go"
)

type Obj struct {
	Lol  int
	Omg  float64
	Haha bool
	Rofl string
}

func TestWindow(t *testing.T) {
	w := NewWindow(300, 300)
	w.AddView(NewLogger("lol"))
	w.AddView(NewLogger("omg"))
	w.AddView(NewLogger("rofl"))
	object := &Obj{Lol: 4, Omg: 2.7, Haha: true, Rofl: "tihi"}
	w.AddView(NewObjectView("", object))
}

func TestTerminal(t *testing.T) {
	Init()
	defer Close()

	log := AddLogger("EA log")
	AddLogger("LOL log")
	object := &Obj{Lol: 4, Omg: 2.7, Haha: true, Rofl: "tihi"}
	window.AddView(NewObjectView("", object))
	window.AddView(NewRuntimeView())
	func() {
		for i := 0; i < 31; i++ {
			if !termbox.IsInit {
				fmt.Println("Early quit!")
				return
			}

			log.Println("HAHAHA......................................................................................................................",
				termbox.ColorGreen, i)
			time.Sleep(50 * time.Millisecond)
		}

		for i := 0; i < 4; i++ {
			if !termbox.IsInit {
				fmt.Println("Early quit!")
				return
			}
			fmt.Println("HAHAHA......................................................................................................................",
				termbox.ColorGreen, "LOL")
			time.Sleep(50 * time.Millisecond)
		}
	}()

	UserExit()
}
