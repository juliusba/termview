package terminal

import "github.com/nsf/termbox-go"

type View interface {
	CellBuffer() cellBuffer
	Render()

	MinWidth() (minWidth int)
	MinHeight() (minHeight int)
	MinSize() (minWidth, minHeight int)

	Width() (width int)
	Height() (height int)
	Size() (width, height int)
	SetSize(width, height int)

	Position() (x, y int)
	SetPosition(x, y int)
	KeyEvent(key termbox.Key) bool
	SetFocus(focus bool)
}
